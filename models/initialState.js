export default {
  messages: [
    {
      channelId: 1,
      userId: "Genesis",
      content: "Welcome to the blockchain chat!",
      //signature: ...,
      timestamp: Date.now(),
    },
  ],
  channels: [
    {
      channelId: 1,
      channelName: "Global",
      channelDescription: "Global root channel of the blockchain chat",
    },
  ],
};
