export default class User {
  userId; // MUST be unique e.g. a public key
  username; // required but may be non-unique
  title;

  constructor(userId, username, title) {
    if (userId === undefined || username === undefined)
      throw new Error("Invalid user");

    this.userId = userId;
    this.username = username;
    this.title = title;
  }
}
