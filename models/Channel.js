export default class Channel {
  channelId;
  channelName;
  channelDescription;

  constructor(channelId, channelName, channelDescription) {
    if (
      channelId === undefined ||
      channelName === undefined ||
      channelDescription === undefined
    )
      throw new Error("Invalid channel");

    this.channelId = channelId;
    this.channelName = channelName;
    this.channelDescription = channelDescription;
  }
}
