export default class Message {
  channelId;
  userId;
  content;
  //signature;
  timestamp;

  constructor(channelId, userId, content, timestamp) {
    if (
      channelId === undefined ||
      userId === undefined ||
      content === undefined ||
      timestamp === undefined
    )
      throw new Error("Invalid message");

    this.channelId = channelId;
    this.userId = userId;
    this.content = content;
    this.timestamp = timestamp;
  }
}
