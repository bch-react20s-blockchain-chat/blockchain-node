import Message from "../models/Message.js";

export const messageHandler = (state, tx) => {
  try {
    // Handle this transaction only if it's about adding messages
    if (tx.channelId && tx.userId && tx.content && tx.timestamp) {
      if (
        state.channels.find((channel) => channel.channelId === tx.channelId)
      ) {
        const message = new Message(
          tx.channelId,
          tx.userId,
          tx.content,
          tx.timestamp
        );
        state.messages.push(message);
      } else new Error("Invalid channel id");
    }
  } catch (error) {
    console.log(error);
  }
};
