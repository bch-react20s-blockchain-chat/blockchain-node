import User from "../models/User.js";

export const userHandler = (state, tx) => {
  try {
    // Handle this transaction only if it's about adding users
    if (tx.userId && tx.username) {
      const user = new User(tx.userId, tx.username, tx.title);
      state.users ? state.users.push(user) : (state.users = [user]);
    }
  } catch (error) {
    console.log(error);
  }
};
