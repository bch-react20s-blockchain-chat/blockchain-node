import Channel from "../models/Channel.js";

export const channelHandler = (state, tx) => {
  try {
    // Handle this transaction only if it's about adding channels
    if (tx.channelName && tx.channelDescription) {
      const channel = new Channel(
        state.channels ? state.channels.length + 1 : tx.channelId,
        tx.channelName,
        tx.channelDescription
      );
      state.channels
        ? state.channels.push(channel)
        : (state.channels = [channel]);
    }
  } catch (error) {
    console.log(error);
  }
};
