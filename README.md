# Blockhain Node

![Demo](./demo.gif)

## Important environment settings
- `.env`
  - `ENVIRONMENT` - defaults to development. Set to `production` to join a persisting blockchain.

- Other files
  - `keys.json` - required to run the node in production mode.

## Available scripts

### `npm start`

Starts the node

### `npm run dev-node`

Starts the node in debug mode

### `npm run dev-client`

Runs lotion-cli that can be used as a test client to the blockchain.  

Usage

````
  Usage:
    $ npm run dev-client state <GCI>             Get the latest state of an app
    $ npm run dev-client send <GCI> <tx-json>    Send a transaction to a running app

  For example:
    $ npm run dev-client state 19bfd8...
    $ npm run dev-client send 19bfd8... '{ "userId": "John", "message": "Hello world!" }
````
