import env from 'dotenv';
import initialState from './models/initialState.js';
import { PRODUCTION } from './constants/constants.js';

env.config();

export const ENVIRONMENT = process.env.ENVIRONMENT;

export const lotionOptions =
  ENVIRONMENT === PRODUCTION
    ? {
        initialState: initialState,
        keyPath: './keys.json',
        genesisPath: './genesis.json',
        p2pPort: process.env.LOTION_P2P_PORT || 26658,
        rpcPort: process.env.LOTION_RPC_PORT || 26657
      }
    : {
        devMode: true,
        initialState: initialState,
        p2pPort: 26658,
        rpcPort: 26657,
      };
