import lotion from 'lotion';
import * as config from './config.js';
import { PRODUCTION } from './constants/constants.js';
import { messageHandler } from './handlers/messageHandler.js';
import { userHandler } from "./handlers/userHandler.js";
import { channelHandler } from "./handlers/channelHandler.js";

const app = lotion(config.lotionOptions);

app.use(messageHandler);
app.use(userHandler);
app.use(channelHandler);

app
  .start()
  .then((status) =>
    config.ENVIRONMENT === PRODUCTION
      ? console.log(`App running with GCI ${status.GCI}`)
      : console.log(status)
  );
